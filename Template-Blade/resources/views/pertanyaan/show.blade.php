@extends('admin.master')
@section('title', 'Show')
@section('content')
    <div class="mx3">
        <div class="m-5">
            <h4>{{$pertanyaan->judul}}</h4>
            <p>{{$pertanyaan->isi}}</p>
            <a href="/pertanyaan" class="btn btn-danger">Back</a>
        </div>
    </div>
@endsection