@extends('admin.master')

@section('content')
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Form Edit Pertanyaan</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->

              <form action="/pertanyaan/{{$pertanyaan->id}}" method="POST">
              @csrf
              @method('put')
                <div class="card-body">
                  <div class="form-group">
                    <label for="judul">Judul</label>
                    <input type="text" class="form-control" id="judul" name="judul" value="{{$pertanyaan->judul}}">
                    @error('judul')
                        <div class="alert alert-danger">
                            {{$message}}
                        </div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="isi">Isi</label>
                    <textarea name="isi" id="isi" class="form-control" cols="30" rows="10">{{$pertanyaan->isi}}</textarea>
                    @error('isi')
                        <div class="alert alert-danger">
                            {{$message}}
                        </div>
                    @enderror
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Update</button>
                </div>
              </form>
            </div>
@endsection

