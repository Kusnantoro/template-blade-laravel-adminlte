@extends('admin.master')

@section('title', 'Data Pertanyaan')
  
@section('content')
<div>
  <a class="btn btn-primary sm m-2" href="/pertanyaan/create">Create Data</a>
</div>

<table class="table table-bordered mt-1">
  <thead>                  
    <tr>
      <th scope="col">#</th>
      <th scope="col">Judul</th>
      <th scope="col">Isi</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($pertanyaan as $key=>$value)
    <tr>
      <td>{{$key + 1}}</td>
      <td>{{$value->judul}}</td>
      <td>{{$value->isi}}</td>
      <td>
        <form action="/pertanyaan/{{$value->id}}" method="POST">
        <a href="/pertanyaan/{{$value->id}}" class="btn btn-info">Show</a>
        <a href="/pertanyaan/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
        @csrf
        @method('DELETE')
        <input type="submit" class="btn btn-danger my-1" value="Delete">
        </form>
      </td>
    </tr>
    @empty
    <tr colspan="3">
        <td>No data</td>
    </tr>
    @endforelse
  </tbody>
</table>
@endsection